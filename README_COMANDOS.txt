git config --global user.name "oscar quijon"
git config --global user.email "oscar.mecanica@gmail.com"

Create a new repository
git clone https://gitlab.com/oscarmecanica/programas_processing.git
cd programas_processing
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/oscarmecanica/programas_processing.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/oscarmecanica/programas_processing.git
git push -u origin --all
git push -u origin --tags